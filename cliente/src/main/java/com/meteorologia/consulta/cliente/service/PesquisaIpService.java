package com.meteorologia.consulta.cliente.service;


import com.meteorologia.consulta.cliente.config.UrlSerices;
import com.meteorologia.consulta.cliente.exception.ApiException;
import com.meteorologia.consulta.cliente.model.ClienteIP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;

import org.springframework.cache.annotation.Caching;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.validation.constraints.NotNull;
import java.util.Optional;


@Service
public class PesquisaIpService {


    @Autowired
    private RestTemplate restTemplate;


    @Autowired
    private UrlSerices urlSerices;


    @Cacheable(value = "ClienteIP",key = "#ip")
    public Optional<ClienteIP> obterIpCliente(@NotNull  String ip){



        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add("User-Agent", "request");
            HttpEntity<String> entity = new HttpEntity<>(headers);

            ResponseEntity<ClienteIP> responseIpVigilante = restTemplate.exchange(urlSerices.getIpvigilante(), HttpMethod.GET, entity, ClienteIP.class, ip);
            return  Optional.ofNullable(responseIpVigilante.getBody());

        }catch (Exception ex){

            throw  new ApiException("Erro ao chamar o serviço de pesquisa de ip", ex);
        }
    }
}
