package com.meteorologia.consulta.cliente.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.web.client.RestTemplate;


@SpringBootApplication
@ComponentScan(basePackages = "com.meteorologia.consulta.cliente.*")
@EnableMongoRepositories(basePackages = "com.meteorologia.consulta.cliente.repository")
@EnableDiscoveryClient
@EnableCaching
public class ClienteApplication {


    @Bean
    RestTemplate restTemplate(){
        return new RestTemplateBuilder().errorHandler(new RestTemplateResponseErrorHandler()).build();
    }


    public static void main(String[] args) {
        SpringApplication.run(ClienteApplication.class, args);
    }
}
