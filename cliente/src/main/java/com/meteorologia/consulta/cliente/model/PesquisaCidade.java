package com.meteorologia.consulta.cliente.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class PesquisaCidade implements Serializable {

    @JsonProperty("title")
    private String titulo;

    @JsonProperty("location_type")
    private String tipoLocalizacao;

    @JsonProperty("woeid")
    private int localID;

    @JsonProperty("latt_long")
    private String latitudeLongitude;

}
