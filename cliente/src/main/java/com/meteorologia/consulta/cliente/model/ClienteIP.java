package com.meteorologia.consulta.cliente.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class ClienteIP  implements Serializable {

    private String status;

    @JsonProperty("data")
    private ClienteIPConteudo data;
}
