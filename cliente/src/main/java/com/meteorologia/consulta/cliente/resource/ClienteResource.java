package com.meteorologia.consulta.cliente.resource;


import com.meteorologia.consulta.cliente.entity.ClienteEntity;
import com.meteorologia.consulta.cliente.exception.ServiceException;
import com.meteorologia.consulta.cliente.model.Cliente;
import com.meteorologia.consulta.cliente.service.ClienteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping(path = "cliente")
public class ClienteResource {


    private static final Logger logger = LoggerFactory.getLogger(ClienteResource.class);

    @Autowired
    private ClienteService clienteService;

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ClienteEntity>  cadastroCliente(@RequestBody Cliente cliente, @RequestHeader("ip") String ip, HttpServletRequest request){


        cliente.setIp(ip);


        return this.salvarAtualizarCliente(cliente);


    }

    @ResponseBody
    @RequestMapping(path = "/{id}",method = RequestMethod.PUT,consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ClienteEntity>  atualizarCliente(@RequestBody Cliente cliente, @RequestHeader("ip") String ip, HttpServletRequest request,@PathVariable("id") String id){


        cliente.setId(id);
        cliente.setIp(ip);


        return this.salvarAtualizarCliente(cliente);

    }

    @ResponseBody
    @RequestMapping(path = "/{id}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ClienteEntity> consultaClienteID(HttpServletRequest request, @PathVariable("id")String id){

        try {
           return new ResponseEntity<>(clienteService.consultaClienteID(id),HttpStatus.OK);
        } catch (Exception ex ) {
            logger.error("Errro ao consultar cliente por id",ex);
        }

        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<ClienteEntity>>  consiltaTodosCliente(HttpServletRequest request){



        try {
            return new ResponseEntity<>(clienteService.consiltaTodosCliente(),HttpStatus.OK);
        } catch (Exception ex ) {
            logger.error("Errro ao consultar cliente por id",ex);
        }

        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.DELETE,path = "/{id}",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity deletarCliente(HttpServletRequest request,@PathVariable("id")String id){


        try {

            clienteService.deletarCliente(id);

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception ex ) {
            logger.error("Errro ao consultar cliente por id",ex);
        }

        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);


    }


    private ResponseEntity<ClienteEntity>  salvarAtualizarCliente(Cliente cliente){

        ClienteEntity clienteEntity = null;

        try {

            clienteEntity = clienteService.cadasttro(cliente);

           return new ResponseEntity<>(clienteEntity,HttpStatus.OK);


        } catch (ServiceException e) {

            logger.error("Erro ao salvar a temperatura",e);
            return new ResponseEntity<>(clienteEntity,HttpStatus.OK);


        }catch (Exception ex){

            logger.error("Erro ao salvar a temperatura",ex);

            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }
}
