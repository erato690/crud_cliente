package com.meteorologia.consulta.cliente.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

@Data
@NoArgsConstructor
public class Clima implements Serializable {

    private String id;

    @JsonProperty("weather_state_name")
    private String estadoNome;

    @JsonProperty("applicable_date")
    private LocalDate data;

    @JsonProperty("min_temp")
    private Double minimuTemperatura;

    @JsonProperty("max_temp")
    private Double maximuTemperatura;


}
