package com.meteorologia.consulta.cliente.entity;

import com.meteorologia.consulta.cliente.model.Clima;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "cliente")
public class ClienteEntity {



    @Id
    private String id;

    private String ip;

    private String nome;

    private int idade;

    private Clima clima;



}
