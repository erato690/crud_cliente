package com.meteorologia.consulta.cliente.service;


import com.meteorologia.consulta.cliente.entity.ClienteEntity;
import com.meteorologia.consulta.cliente.exception.ApiException;
import com.meteorologia.consulta.cliente.exception.ServiceException;
import com.meteorologia.consulta.cliente.model.*;
import com.meteorologia.consulta.cliente.repository.ClienteRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private ClimaService climaService;

    @Autowired
    private PesquisaIpService pesquisaIpService;


    public ClienteEntity cadasttro(Cliente cliente) throws ServiceException {

        ClienteEntity clienteEntity = new ClienteEntity();
        BeanUtils.copyProperties(cliente,clienteEntity);

        try{

            Optional<ClienteIP> clienteIP = pesquisaIpService.obterIpCliente(cliente.getIp());

            if(clienteIP.isPresent()){

                PesquisaCidade pesquisaCidade =  climaService.pesquisarCodigoCidedade(clienteIP.get().getData().getNomeCidade());

                clienteEntity.setClima(climaService.temperaturaDiaCidade(pesquisaCidade.getLocalID()));
            }


            clienteRepository.save(clienteEntity);


        }catch (ApiException apiEx){

            apiEx.printStackTrace();

            clienteEntity.setClima(new Clima());
            clienteRepository.save(clienteEntity);

        }catch (Exception ex){

            ex.printStackTrace();
        }


        return clienteEntity;

    }



    public ClienteEntity consultaClienteID(String id){
        return clienteRepository.findById(id).get();
    }

    public List<ClienteEntity> consiltaTodosCliente(){
           return  clienteRepository.findAll();
    }


    public void deletarCliente(String id){

        clienteRepository.deleteById(id);
    }



}
