package com.meteorologia.consulta.cliente.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
public class ClimaCidade implements Serializable {


    @JsonProperty("consolidated_weather")
    private List<Clima> listaClimaCidade;
}
