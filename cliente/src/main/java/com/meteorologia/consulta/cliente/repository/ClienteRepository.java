package com.meteorologia.consulta.cliente.repository;


import com.meteorologia.consulta.cliente.entity.ClienteEntity;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface ClienteRepository extends MongoRepository<ClienteEntity,String> {

}
