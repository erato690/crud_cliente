package com.meteorologia.consulta.cliente.service;


import com.meteorologia.consulta.cliente.config.UrlSerices;
import com.meteorologia.consulta.cliente.exception.ApiException;
import com.meteorologia.consulta.cliente.model.ClienteIP;
import com.meteorologia.consulta.cliente.model.Clima;
import com.meteorologia.consulta.cliente.model.ClimaCidade;
import com.meteorologia.consulta.cliente.model.PesquisaCidade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import java.time.LocalDate;
import java.util.List;

@Service
public class ClimaService {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private UrlSerices urlSerices;



    @Cacheable(value = "PesquisaCidade",key = "#nomeCidade")
    public PesquisaCidade pesquisarCodigoCidedade(String nomeCidade) {

        try {

            HttpHeaders headers = new HttpHeaders();
            headers.add("User-Agent","request");
            HttpEntity<String> entity = new HttpEntity<>(headers);


            ResponseEntity<List<PesquisaCidade>> responsePesquisaCidade = restTemplate.exchange(urlSerices.getProcurarCodigopoCidade(),HttpMethod.GET,entity,new ParameterizedTypeReference<List<PesquisaCidade>>(){},nomeCidade);

            return responsePesquisaCidade.getBody().get(0);

        }catch (Exception ex){

            throw  new ApiException("Erro ao chamar a servico de pesquisa de codigo por cidade",ex);
        }



    }

    @Caching(put = {
            @CachePut(value = "Clima",key = "#codigoCidade")
    },
    cacheable = {
            @Cacheable(value = "Clima",key = "#codigoCidade")
    })
    public Clima temperaturaDiaCidade(int codigoCidade){

        try {

            HttpHeaders headers = new HttpHeaders();
            headers.add("User-Agent","request");
            HttpEntity<String> entity = new HttpEntity<>(headers);


            ResponseEntity<ClimaCidade> responsePesquisaTemperaturaCidade = restTemplate.exchange(urlSerices.getTemperaturaLocal(),HttpMethod.GET,entity,ClimaCidade.class,codigoCidade);

              return responsePesquisaTemperaturaCidade.getBody()
                        .getListaClimaCidade()
                        .stream()
                        .filter(climaCidade -> climaCidade.getData().equals(LocalDate.now()))
                        .findFirst().orElse( new Clima());




        }catch (Exception ex){

            throw  new ApiException("Erro ao chamar a servico de pesquisa de codigo por cidade",ex);
        }



    }
}
