package com.meteorologia.consulta.cliente.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class ClienteIPConteudo implements Serializable {

    @JsonProperty("city_name")
    private String nomeCidade;
}
