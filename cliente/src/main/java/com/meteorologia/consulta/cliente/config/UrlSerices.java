package com.meteorologia.consulta.cliente.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


@Component
@Data
@ConfigurationProperties(prefix = "api.externa")
public class UrlSerices {


    private String ipvigilante;
    private String procurarCodigopoCidade;

    private String temperaturaLocal;

}
