package com.meteorologia.consulta.cliente.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class Cliente implements Serializable {

    private String nome;
    private int idade;

    @JsonIgnore
    private String ip;

    @JsonIgnore
    private String id;


}
