package com.meteorologia.consulta.cliente.config;


import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;
import com.meteorologia.consulta.cliente.exception.ApiException;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;
import java.io.IOException;
import java.io.InputStreamReader;



public class RestTemplateResponseErrorHandler implements ResponseErrorHandler {

    @Override
    public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
        return clientHttpResponse.getStatusCode().isError();
    }

    @Override
    public void handleError(ClientHttpResponse clientHttpResponse) throws IOException {

        if(clientHttpResponse.getStatusCode().isError()){

            String content = CharStreams.toString(new InputStreamReader(clientHttpResponse.getBody(), Charsets.UTF_8));
            throw new ApiException("Erro ao chamar a api: "+ content);
        }
    }
}