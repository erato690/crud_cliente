É necessário as seguintes aplicações de terceiros:
mongoDB
Redis
RabbitMQ

No mongo é necessário criar uma database com o nome info_cliente e uma collection com o nome de cliente.
Caso esteja as aplicações em portas e host diferentes é possível alterar os valores.

Estou usando spring-cloud:

Spring-config:
Para armazenar no repositório git os arquivos de configuração do sistema, ele está dividido por branch e profile sendo cada profile para um branch separada.
Uso o rabbitmq para fazer o auto-refresh dos arquivos de propriedades.
Será necessário criar um repositório para guarda os arquivos de configuração.

Spring-cloud-eureka:
Service discover da netflix usado para registrar as aplicações como config,zull,e o client aqui tambem é possivel ver a saude do serviço.

Spring-cloud-zuul:
Ponto de entrada para as chamadas da api  também faz um balance escolhendo para qual  serve vai para passar a requisição.

Escolhi essa arquitetura por que para adicionar novos cluster do projeto é simples apenas necessitando subir uma nova instância do projeto cliente.
Com o zuul e eureka temos balanciamentos e check Heart dos serviços sendo mais fácil verificar quando algum serviço caiu. 

Para usar os serviços basta apenas acessar o swagger, ele pode ser acessado pelo api Gateway ou diretamente.Para acessar pelo api Gateway é necessário subir o projeto 
Service-gateway  pode utilizar o seguinte comando:

 java -jar {nome do jar gerado}.jar -Dport={mudar a porta default}  --spring.profiles.actives={profile do ambiente}.

A propriedade -Dport é usada para passar um novo valor para   de porta para a aplicação.

Para rodar o ambiente é necessário subir o projeto:


Config
Eureka
Zuul
-Apicações

O swagger pode ser acessado pelo :

http://{host}/crud_cliente/swagger-ui.html

O host é o do zuul como estava subindo localhost aqui estava:
http://localhost:8296/crud_cliente/swagger-ui.html

Para gerar os .jar basta rodar o comando:

 mvn packge na raiz de cada projeto o jar vai aparecer dentro da pasta target.


Para  seria ideal rodar um jenkins para fazer o build da aplicação  e rodar os teste de regressão e unitário, depois executar as próximas tarefas do pipeline criando container dockers para publicar as aplicações em produção.

